<?php

namespace Caravana\API\Models\Requests\Contracts;


interface Validatable
{
    function validate();
}