<?php

namespace Caravana\API\Models\Requests\OAuth\Contracts;


interface CreateAccessTokenRequest extends \JsonSerializable
{
    function getClientId();
    function setClientId($client_id);
    function getClientSecret();
    function setClientSecret($client_secret);
    function getGrantType();
    function setGrantType($grant_type);
    function getUsername();
    function setUsername($username);
    function getPassword();
    function setPassword($password);
}