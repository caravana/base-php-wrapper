<?php

namespace Caravana\API\Models\Requests\OAuth;


use Caravana\API\Models\Requests\Contracts\Validatable;
use Caravana\API\Models\Requests\OAuth\Base\BaseCreateAccessTokenRequest;
use jamesvweston\Utilities\ArrayUtil AS AU;

class CreateAccessTokenRequest extends BaseCreateAccessTokenRequest implements Validatable
{

    /**
     * AccessToken constructor.
     * @param array|null $data
     */
    public function __construct($data = null)
    {
        if (is_array($data))
        {
            $this->client_id                    = AU::get($data['client_id']);
            $this->client_secret                = AU::get($data['client_secret']);
            $this->grant_type                   = AU::get($data['grant_type'], 'password');
            $this->username                     = AU::get($data['username']);
            $this->password                     = AU::get($data['password']);
        }
    }

    public function jsonSerialize()
    {
        return [
            [
                'name'     => 'client_id',
                'contents' => $this->client_id,
            ],
            [
                'name'     => 'client_secret',
                'contents' => $this->client_secret,
            ],
            [
                'name'     => 'username',
                'contents' => $this->username,
            ],
            [
                'name'     => 'password',
                'contents' => $this->password,
            ],
            [
                'name'     => 'grant_type',
                'contents' => $this->grant_type,
            ]
        ];
    }

    public function validate()
    {
        // TODO: Implement validate() method.
    }

}