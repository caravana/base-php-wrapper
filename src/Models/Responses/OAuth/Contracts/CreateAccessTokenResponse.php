<?php

namespace Caravana\API\Models\Responses\OAuth\Contracts;


interface CreateAccessTokenResponse extends \JsonSerializable
{
    function getAccessToken();
    function setAccessToken($access_token);
    function getTokenType();
    function setTokenType($token_type);
    function getExpiresIn();
    function setExpiresIn($expires_in);
}