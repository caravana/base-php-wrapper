<?php

namespace Caravana\API\Configuration\Contracts;


interface APIConfiguration extends \JsonSerializable
{
    function getUsername();
    function setUsername($username);
    function getPassword();
    function setPassword($password);
    function getClientId();
    function setClientId($clientId);
    function getClientSecret();
    function setClientSecret($clientSecret);
    function getGrantType();
    function setGrantType($grantType);
    function getAccessToken();
    function setAccessToken($accessToken);
    function getAuthEndpoint();
    function setAuthEndPoint($authEndPoint);
}