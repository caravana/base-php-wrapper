<?php

namespace Caravana\API\Configuration;


use Caravana\API\Configuration\Base\BaseAPIConfiguration;
use Caravana\API\Exceptions\RequiredFieldMissingException;
use Caravana\API\Models\Requests\Contracts\Validatable;
use jamesvweston\Utilities\ArrayUtil AS AU;

class APIConfiguration extends BaseAPIConfiguration implements Validatable
{

    /**
     * APIConfiguration constructor.
     * @param   array|null      $data
     */
    public function __construct($data = null)
    {
        $this->username             = AU::get($data['username']);
        $this->password             = AU::get($data['password']);
        $this->clientId             = AU::get($data['clientId']);
        $this->clientSecret         = AU::get($data['clientSecret']);
        $this->grantType            = AU::get($data['grantType'], 'password');
        $this->accessToken          = AU::get($data['accessToken']);
        $this->authEndPoint         = AU::get($data['authEndPoint'], 'https://caravana.rentals');
        $this->apiEndPoint          = AU::get($data['apiEndPoint'], 'https://api.caravana.rentals');
    }

    /**
     * @throws RequiredFieldMissingException
     */
    public function validate()
    {
        //  Validate that everything is set but the accessToken
        if (empty($this->username))
            throw new RequiredFieldMissingException('ApiConfiguration', 'username');
        else if (empty($this->password))
            throw new RequiredFieldMissingException('ApiConfiguration', 'password');
        else if (empty($this->clientId))
            throw new RequiredFieldMissingException('ApiConfiguration', 'clientId');
        else if (empty($this->clientSecret))
            throw new RequiredFieldMissingException('ApiConfiguration', 'clientSecret');
        else if (empty($this->grantType))
            throw new RequiredFieldMissingException('ApiConfiguration', 'grantType');
        else if (empty($this->authEndPoint))
            throw new RequiredFieldMissingException('ApiConfiguration', 'authEndPoint');
        else if (empty($this->apiEndPoint))
            throw new RequiredFieldMissingException('ApiConfiguration', 'apiEndPoint');
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        return [
            'username'              => $this->username,
            'password'              => $this->password,
            'clientId'              => $this->clientId,
            'clientSecret'          => $this->clientSecret,
            'grantType'             => $this->grantType,
            'accessToken'           => $this->accessToken,
            'authEndPoint'          => $this->authEndPoint,
            'apiEndPoint'           => $this->apiEndPoint,
        ];
    }

}