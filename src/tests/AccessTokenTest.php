<?php

namespace Caravana\API\Tests;


use Caravana\API\Api;
use Dotenv\Dotenv;

/**
 * Class AccessTokenTest
 * @package Caravana\API\Tests
 */
class AccessTokenTest extends \PHPUnit_Framework_TestCase
{

    public function testENVInstantiation()
    {
        $api                = new Api('./');
        $this->assertInstanceOf('Caravana\API\Api', $api);
    }
    
    public function testArrayInstantiation()
    {
        $dotEnv                         = new Dotenv('./');
        $dotEnv->load();
        
        $data = [
            'username'                  => getenv('CARAVANA_USERNAME'),
            'password'                  => getenv('CARAVANA_PASSWORD'),
            'clientId'                  => getenv('CARAVANA_CLIENT_ID'),
            'clientSecret'              => getenv('CARAVANA_CLIENT_SECRET'),
            'grantType'                 => getenv('CARAVANA_GRANT_TYPE'),
            'accessToken'               => getenv('CARAVANA_ACCESS_TOKEN'),
            'authEndPoint'              => getenv('CARAVANA_AUTH_ENDPOINT'),
            'apiEndPoint'               => getenv('CARAVANA_API_ENDPOINT'),
        ];
        
        $api                = new Api($data);
        $this->assertInstanceOf('Caravana\API\Api', $api);
    }
    
    public function testOAuthToken()
    {
        $api                = new Api('./');
        $result             = $api->requestAccessToken(true);
        $this->assertInstanceOf('Caravana\API\Models\Responses\OAuth\CreateAccessTokenResponse', $result);
    }
}