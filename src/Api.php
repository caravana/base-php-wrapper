<?php

namespace Caravana\API;


use Caravana\API\Configuration\APIConfiguration;
use Caravana\API\Exceptions\RequiredFieldMissingException;
use Caravana\API\Exceptions\UnauthorizedClientException;
use Caravana\API\Http\HttpRequest;
use Caravana\API\Models\Responses\OAuth\CreateAccessTokenResponse;
use Dotenv\Dotenv;
use GuzzleHttp\Exception\ConnectException;
use GuzzleHttp\Exception\RequestException;
use jamesvweston\Utilities\ArrayUtil AS AU;


class Api
{
    
    /**
     * @var         APIConfiguration
     */
    protected $apiConfiguration;

    /**
     * @var         HttpRequest
     */
    protected $httpRequest;
    

    /**
     * AuthClient constructor.
     * @param       string|array    $config
     * @throws      \InvalidArgumentException
     * @throws      \Exception
     */
    public function __construct($config)
    {
        if (is_string($config))
        {
            if (!is_dir($config)) 
                throw new \Exception('The provided directory location does not exist at ' . $config);

            $dotEnv                         = new Dotenv($config);
            $dotEnv->load();
            
            $data = [
                'username'                  => getenv('CARAVANA_USERNAME'),
                'password'                  => getenv('CARAVANA_PASSWORD'),
                'clientId'                  => getenv('CARAVANA_CLIENT_ID'),
                'clientSecret'              => getenv('CARAVANA_CLIENT_SECRET'),
                'grantType'                 => getenv('CARAVANA_GRANT_TYPE'),
                'authEndPoint'              => getenv('CARAVANA_AUTH_ENDPOINT'),
                'apiEndPoint'               => getenv('CARAVANA_API_ENDPOINT'),
                'accessToken'               => getenv('CARAVANA_ACCESS_TOKEN'),
            ];
        } else {
            if (is_array($config)) {
                $data = [
                    'username'              => AU::get($config['username']),
                    'password'              => AU::get($config['password']),
                    'clientId'              => AU::get($config['clientId']),
                    'clientSecret'          => AU::get($config['clientSecret']),
                    'grantType'             => AU::get($config['grantType']),
                    'authEndPoint'          => AU::get($config['authEndPoint']),
                    'apiEndPoint'           => AU::get($config['apiEndPoint']),
                    'accessToken'           => AU::get($config['accessToken']),
                ];
            } else {
                throw new \InvalidArgumentException('A configuration must be provided');
            }
        }
        
        $this->apiConfiguration             = new APIConfiguration($data);
        $this->httpRequest                  = new HttpRequest($this->apiConfiguration);
    }

    
    /**
     * @param       string          $url            The relative URL after the host name
     * @param       array|null      $queryString    Data to add as a queryString to the url
     * @return      mixed
     * @throws      ConnectException
     * @throws      RequestException
     * @throws      UnauthorizedClientException
     * @throws      \Exception
     */
    public function get($url, $queryString = null)
    {
        return $this->tryRequest('get', $url, null, $queryString);
    }

    /**
     * @param       string          $url            The relative URL after the host name
     * @param       array           $payload        The relative URL after the host name
     * @param       array|null      $queryString    Data to add as a queryString to the url
     * @return      mixed
     * @throws      ConnectException
     * @throws      RequestException
     * @throws      UnauthorizedClientException
     * @throws      \Exception
     */
    public function post($url, $payload, $queryString = null)
    {
        return $this->tryRequest('post', $url, $payload, $queryString);
    }

    /**
     * @param       string          $url            The relative URL after the host name
     * @param       array           $payload        The relative URL after the host name
     * @param       array|null      $queryString    Data to add as a queryString to the url
     * @return      mixed
     * @throws      ConnectException
     * @throws      RequestException
     * @throws      UnauthorizedClientException
     * @throws      \Exception
     */
    public function put($url, $payload, $queryString = null)
    {
        return $this->tryRequest('put', $url, $payload, $queryString);
    }

    /**
     * @param       string          $url            The relative URL after the host name
     * @param       array|null      $queryString    Data to add as a queryString to the url
     * @return      mixed
     * @throws      ConnectException
     * @throws      RequestException
     * @throws      UnauthorizedClientException
     * @throws      \Exception
     */
    public function delete($url, $queryString = null)
    {
        return $this->tryRequest('delete', $url, null, $queryString);
    }

    /**
     * @param       string          $method         The Http verb
     * @param       string          $url            The relative URL after the host name
     * @param       array|null      $payload        Contents of the body
     * @param       array|null      $queryString    Data to add as a queryString to the url
     * @param       bool            $firstTry
     * @return      mixed
     * @throws      ConnectException
     * @throws      RequestException
     * @throws      UnauthorizedClientException
     * @throws      \Exception
     */
    protected function tryRequest($method, $url, $payload = null, $queryString = null, $firstTry = true)
    {
        try
        {
            if (empty($this->apiConfiguration->getAccessToken()))
                $this->requestAccessToken();
            return $this->httpRequest->makeHttpRequest($method, $url, $payload, $queryString);
        }
        catch (ConnectException $c)
        {
            throw $c;
        }
        catch (RequestException $e)
        {
            if ($e->getResponse()->getStatusCode() == 401)
            {
                if ($firstTry)
                {
                    $this->requestAccessToken();
                    return $this->tryRequest($method, $url, $payload, $queryString, false);
                } else {
                    //something else is wrong and requesting a new token isn't going to fix it
                    throw new \Exception('The request was unauthorized and could not be fixed by refreshing access token.', 0, $e);
                }
            } else {
                throw $e;
            }
        }
    }

    /**
     * @param       bool            $returnResponse     If true the CreateAccessTokenResponse will be returned
     * @return      CreateAccessTokenResponse
     * @throws      RequiredFieldMissingException|UnauthorizedClientException
     */
    public function requestAccessToken($returnResponse = false)
    {
        $accessTokenResponse        = $this->httpRequest->requestAccessToken();
        $this->apiConfiguration->setAccessToken($accessTokenResponse->getAccessToken());
        $this->httpRequest->setApiConfiguration($this->apiConfiguration);

        if ($returnResponse)
            return $accessTokenResponse;
        else
            return null;
    }

    /**
     * @return      APIConfiguration
     */
    public function getApiConfiguration()
    {
        return $this->apiConfiguration;
    }

    /**
     * @param       APIConfiguration      $apiConfiguration
     */
    public function setApiConfiguration(APIConfiguration $apiConfiguration)
    {
        $this->apiConfiguration = $apiConfiguration;
    }
    
}