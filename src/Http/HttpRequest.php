<?php

namespace Caravana\API\Http;


use Caravana\API\Models\Requests\OAuth\CreateAccessTokenRequest;
use Caravana\API\Models\Responses\OAuth\CreateAccessTokenResponse;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ConnectException;
use GuzzleHttp\Exception\RequestException;
use Caravana\API\Configuration\APIConfiguration;
use Caravana\API\Exceptions\RequiredFieldMissingException;
use Caravana\API\Exceptions\UnauthorizedClientException;

class HttpRequest
{

    /**
     * @var APIConfiguration
     */
    protected $apiConfiguration;
    
    /**
     * @var Client
     */
    protected $guzzle;

    /**
     * Request constructor.
     * @param   APIConfiguration $apiConfiguration
     */
    public function __construct(APIConfiguration $apiConfiguration)
    {
        $this->apiConfiguration     = $apiConfiguration;
        $this->guzzle               = new Client();
    }

    /**
     * @return      CreateAccessTokenResponse
     * @throws      RequiredFieldMissingException|UnauthorizedClientException
     */
    public function requestAccessToken()
    {
        //  This can throw a RequiredFieldMissingException
        $this->apiConfiguration->validate();
        
        $request                    = new CreateAccessTokenRequest();
        $request->setClientId($this->apiConfiguration->getClientId());
        $request->setClientSecret($this->apiConfiguration->getClientSecret());
        $request->setGrantType($this->apiConfiguration->getGrantType());
        $request->setUsername($this->apiConfiguration->getUsername());
        $request->setPassword($this->apiConfiguration->getPassword());
        
        $authEndPoint               = $this->apiConfiguration->getAuthEndPoint() . '/oauth/access_token';
        
        try 
        {
            $response               = $this->guzzle->post($authEndPoint,
                ['multipart' => $request->jsonSerialize()],
                ['http_errors' => false]
            );
        } 
        catch (RequestException $e)
        {
            throw new UnauthorizedClientException($e->getMessage());
        }

        $accessTokenJson            = json_decode($response->getBody(), true);
        $accessTokenResponse        = new CreateAccessTokenResponse($accessTokenJson);
        return $accessTokenResponse;
    }

    /**
     * Make a Http Request
     * @param       string          $method         The Http verb
     * @param       string          $url            The relative URL after the host name
     * @param       array|null      $apiRequest     Contents of the body
     * @param       array|null      $queryString    Data to add as a queryString to the url
     * @return      mixed
     * @throws      RequiredFieldMissingException
     * @throws      ConnectException
     * @throws      RequestException
     * @throws      \Exception
     */
    public function makeHttpRequest($method, $url, $apiRequest = null, $queryString = null)
    {
        $this->apiConfiguration->validate();

        $urlEndPoint = $this->apiConfiguration->getApiEndPoint() . '/' . $url;
        
        $data = [
            'headers' => [
                'Authorization' => 'Bearer ' . $this->apiConfiguration->getAccessToken()
            ],
            'json'    => $apiRequest,
            'query'   => $queryString
        ];

        try {
            switch ($method) {
                case 'post':
                    $response = $this->guzzle->post($urlEndPoint, $data);
                    break;
                case 'put':
                    $response = $this->guzzle->put($urlEndPoint, $data);
                    break;
                case 'delete':
                    $response = $this->guzzle->delete($urlEndPoint, $data);
                    break;
                case 'get':
                    $response = $this->guzzle->get($urlEndPoint, $data);
                    break;
                default:
                    throw new \Exception('Missing request method');

            }

            if (in_array(current($response->getHeader('Content-Type')), ['image/png','image/jpg']))
                $result = $response->getBody()->getContents(); 
            else 
            {
                $result = json_decode($response->getBody(), true);
            }

            return $result;

        } catch (ConnectException $c) {
            throw $c;
        } catch (RequestException $e) {
            throw $e;
        }
    }

    /**
     * @return      APIConfiguration
     */
    public function getApiConfiguration()
    {
        return $this->apiConfiguration;
    }

    /**
     * @param       APIConfiguration    $apiConfiguration
     */
    public function setApiConfiguration($apiConfiguration)
    {
        $this->apiConfiguration = $apiConfiguration;
    }
    
}