<?php

namespace Caravana\API\Exceptions\Http;

class HttpNotFoundException extends CaravanaHttpException
{

    /**
     * @param   string              $message
     * @param   string              $shortMessage
     * @param   \Exception|null     $previous
     */
    public function __construct($message = 'Not found', $shortMessage = 'Not found', \Exception $previous = null)
    {
        if (is_null($this->exceptionName))
            $this->exceptionName    = (new \ReflectionClass($this))->getShortName();
        
        parent::__construct($message, 404, $shortMessage, $previous);
    }
    
}