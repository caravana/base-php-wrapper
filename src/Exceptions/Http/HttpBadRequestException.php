<?php

namespace Caravana\API\Exceptions\Http;


class HttpBadRequestException extends CaravanaHttpException
{

    /**
     * @param   string              $message
     * @param   string              $shortMessage
     * @param   \Exception|null     $previous
     */
    public function __construct($message = 'Bad request', $shortMessage = 'Bad request', \Exception $previous = null)
    {
        if (is_null($this->exceptionName))
            $this->exceptionName    = (new \ReflectionClass($this))->getShortName();
        
        parent::__construct($message, 400, $shortMessage, $previous);
    }
    
}