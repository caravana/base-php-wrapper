<?php

namespace Caravana\API\Exceptions\Http;


class CaravanaHttpException extends \Exception implements \JsonSerializable
{

    /**
     * @var string
     */
    protected $exceptionName;

    /**
     * @var string
     */
    protected $shortMessage;
    
    /**
     * @param   string              $message
     * @param   int                 $code
     * @param   string|null         $shortMessage
     * @param   \Exception|null     $previous
     */
    public function __construct($message, $code, $shortMessage = null, \Exception $previous = null)
    {
        if (is_null($this->exceptionName))
            $this->exceptionName    = (new \ReflectionClass($this))->getShortName();
        
        $this->shortMessage         = $shortMessage;

        parent::__construct($message, $code, $previous);
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        $object['exceptionName']    = $this->exceptionName;
        $object['code']             = $this->code;
        $object['message']          = $this->message;
        $object['shortMessage']     = $this->shortMessage;

        return $object;
    }

    /**
     * @return string
     */
    public function getExceptionName()
    {
        return $this->exceptionName;
    }

    /**
     * @return string
     */
    public function getShortMessage()
    {
        return $this->shortMessage;
    }
    
}