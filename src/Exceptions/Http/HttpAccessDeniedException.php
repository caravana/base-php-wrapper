<?php

namespace Caravana\API\Exceptions\Http;


class HttpAccessDeniedException extends CaravanaHttpException
{

    /**
     * @param   string              $message
     * @param   string              $shortMessage
     * @param   \Exception|null     $previous
     */
    public function __construct($message = 'Access denied', $shortMessage = 'Access denied', \Exception $previous = null)
    {
        if (is_null($this->exceptionName))
            $this->exceptionName    = (new \ReflectionClass($this))->getShortName();

        parent::__construct($message, 403, $shortMessage, $previous);
    }
    
}