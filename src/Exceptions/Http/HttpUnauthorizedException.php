<?php

namespace Caravana\API\Exceptions\Http;


class HttpUnauthorizedException extends CaravanaHttpException
{

    /**
     * @param   string              $message
     * @param   string              $shortMessage
     * @param   \Exception|null     $previous
     */
    public function __construct($message = 'Unauthorized', $shortMessage = 'Unauthorized', \Exception $previous = null)
    {
        if (is_null($this->exceptionName))
            $this->exceptionName    = (new \ReflectionClass($this))->getShortName();
        
        parent::__construct($message, 401, $shortMessage, $previous);
    }
    
}