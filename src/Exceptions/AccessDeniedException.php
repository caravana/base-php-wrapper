<?php

namespace Caravana\API\Exceptions;


use Caravana\API\Exceptions\Http\HttpAccessDeniedException;

class AccessDeniedException extends HttpAccessDeniedException
{

    /**
     * AccessDeniedException constructor.
     * @param   string          $message
     * @param   string          $shortMessage
     * @param   \Exception|null $previous
     */
    public function __construct($message = 'Access denied', $shortMessage = 'Access denied', \Exception $previous = null)
    {
        if (is_null($this->exceptionName))
            $this->exceptionName    = (new \ReflectionClass($this))->getShortName();
        
        parent::__construct($message, $shortMessage, $previous);
    }

}