<?php

namespace Caravana\API\Exceptions\Factories;
use Caravana\API\Exceptions\AccessDeniedException;
use Caravana\API\Exceptions\EntityNotFoundException;
use Caravana\API\Exceptions\EntityValidationException;
use Caravana\API\Exceptions\Http\CaravanaHttpException;
use Caravana\API\Exceptions\RequiredFieldMissingException;
use Caravana\API\Exceptions\UnauthorizedClientException;
use GuzzleHttp\Exception\RequestException;
use jamesvweston\Utilities\ArrayUtil AS AU;

class CaravanaExceptionFactory
{

    /**
     * @param   RequestException $ex
     * @return  AccessDeniedException|EntityNotFoundException|EntityValidationException|RequiredFieldMissingException|UnauthorizedClientException|CaravanaHttpException
     */
    public static function parseRequestException(RequestException $ex)
    {
        $code                       = $ex->getCode();
        $body                       = json_decode($ex->getResponse()->getBody()->getContents(), true);
        
        
        if (CaravanaExceptionFactory::isEntityValidationException($code, $body))
        {
            return new EntityValidationException(AU::get($body['Entity']), AU::get($body['field']), AU::get($body['reason']), AU::get($body['providedValue']), $ex->getPrevious());
        }
        else if (CaravanaExceptionFactory::isRequiredFieldMissingException($code, $body))
        {
            return new RequiredFieldMissingException(AU::get($body['Entity']), AU::get($body['field']), $ex->getPrevious());
        }
        else if (CaravanaExceptionFactory::isUnauthorizedClientException($code, $body))
        {
            return new UnauthorizedClientException($ex->getMessage(), $ex->getPrevious());
        }
        else if (CaravanaExceptionFactory::isAccessDeniedException($code, $body))
        {
            return new AccessDeniedException($ex->getMessage(), $ex->getPrevious());
        }
        else if (CaravanaExceptionFactory::isEntityNotFoundException($code, $body))
        {
            return new EntityNotFoundException(AU::get($body['Entity']), AU::get($body['field']), AU::get($body['providedValue']), $ex->getPrevious());
        }
        
        //  Give up and default it to CaravanaException
        return new CaravanaHttpException($ex->getMessage(), $ex->getCode(), null, $ex->getPrevious());
    }

    /**
     * @param   int     $code
     * @param   array   $body
     * @return  bool
     */
    public static function isEntityValidationException($code, $body)
    {
        if ($code == 400 && AU::get($body['exceptionName']) == 'EntityValidationException')
            return true;
        return false;
    }

    /**
     * @param   int     $code
     * @param   array   $body
     * @return  bool
     */
    public static function isRequiredFieldMissingException($code, $body)
    {
        if ($code == 400 && AU::get($body['exceptionName']) == 'RequiredFieldMissingException')
            return true;
        return false;
    }

    /**
     * @param   int     $code
     * @param   array   $body
     * @return  bool
     */
    public static function isUnauthorizedClientException($code, $body)
    {
        if ($code == 401 && AU::get($body['exceptionName']) == 'UnauthorizedClientException')
            return true;
        return false;
    }

    /**
     * @param   int     $code
     * @param   array   $body
     * @return  bool
     */
    public static function isAccessDeniedException($code, $body)
    {
        if ($code == 403 && AU::get($body['exceptionName']) == 'AccessDeniedException')
            return true;
        return false;
    }

    /**
     * @param   int     $code
     * @param   array   $body
     * @return  bool
     */
    public static function isEntityNotFoundException($code, $body)
    {
        if ($code == 404 && AU::get($body['exceptionName']) == 'EntityNotFoundException')
            return true;
        return false;
    }
    
}