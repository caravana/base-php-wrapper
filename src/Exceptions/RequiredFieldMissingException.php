<?php

namespace Caravana\API\Exceptions;


use Caravana\API\Exceptions\Http\HttpBadRequestException;

class RequiredFieldMissingException extends HttpBadRequestException
{

    /**
     * @var     string
     */
    protected $entity;

    /**
     * @var     string
     */
    protected $field;
    
    /**
     * RequiredFieldMissingException constructor.
     * @param   string      $entity
     * @param   int         $field
     * @param   \Exception|null $previous
     */
    public function __construct($entity, $field, \Exception $previous = null)
    {
        if (is_null($this->exceptionName))
            $this->exceptionName    = (new \ReflectionClass($this))->getShortName();
        
        $this->entity           = $entity;
        $this->field            = $field;
        $message                = $this->entity . ':' . $this->field . ' is required';
        $shortMessage           = 'Required';
        
        parent::__construct($message, $shortMessage, $previous);
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        $object                 = parent::jsonSerialize();
        $object['entity']       = $this->entity;
        $object['field']        = $this->field;

        return $object;
    }

    /**
     * @return string
     */
    public function getEntity()
    {
        return $this->entity;
    }

    /**
     * @return string
     */
    public function getField()
    {
        return $this->field;
    }

}