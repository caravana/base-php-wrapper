<?php

namespace Caravana\API\Exceptions;


use Caravana\API\Exceptions\Http\HttpNotFoundException;

class EntityNotFoundException extends HttpNotFoundException implements \JsonSerializable
{

    /**
     * @var     string
     */
    protected $entity;

    /**
     * @var     string
     */
    protected $field;

    /**
     * @var     string
     */
    protected $providedValue;
    
    /**
     * EntityNotFoundException constructor.
     * @param   string      $entity
     * @param   string      $field
     * @param   string|null $providedValue
     * @param   \Exception|null $previous
     */
    public function __construct($entity, $field, $providedValue, \Exception $previous = null)
    {
        if (is_null($this->exceptionName))
            $this->exceptionName    = (new \ReflectionClass($this))->getShortName();
        
        $this->entity           = $entity;
        $this->field            = $field;
        $this->providedValue    = $providedValue;
        
        $message                = $entity . ': ' . $field . ' (' . $providedValue . ') ' . $this->shortMessage;

        parent::__construct($message, 'Not found', $previous);
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        $object                 = parent::jsonSerialize();
        $object['entity']       = $this->entity;
        $object['field']        = $this->field;
        $object['providedValue']= $this->providedValue;
        
        return $object;
    }

    /**
     * @return string
     */
    public function getEntity()
    {
        return $this->entity;
    }

    /**
     * @return string
     */
    public function getField()
    {
        return $this->field;
    }

    /**
     * @return string
     */
    public function getProvidedValue()
    {
        return $this->providedValue;
    }
    
}