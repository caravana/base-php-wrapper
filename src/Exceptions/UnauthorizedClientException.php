<?php

namespace Caravana\API\Exceptions;


use Caravana\API\Exceptions\Http\HttpUnauthorizedException;

class UnauthorizedClientException extends HttpUnauthorizedException implements \JsonSerializable
{
    
    /**
     * @param   string              $message
     * @param   string              $shortMessage
     * @param   \Exception|null     $previous
     */
    public function __construct($message = 'Unable to authenticate', $shortMessage = 'Unable to authenticate', \Exception $previous = null)
    {
        if (is_null($this->exceptionName))
            $this->exceptionName    = (new \ReflectionClass($this))->getShortName();
        
        parent::__construct($message, $shortMessage, $previous);
    }

}